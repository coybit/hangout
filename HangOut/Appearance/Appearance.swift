//
//  Appearance.swift
//  HangOut
//
//  Created by Mohsen Alijanpour on 18/03/2020.
//  Copyright © 2020 mohsen.dev. All rights reserved.
//

import UIKit

class Appearance {
    static var backgroundColor: UIColor {
        if #available(iOS 13.0, *) {
            return UIColor.systemGray6
        } else {
            return .white
        }
    }
    
    static var ghostColor: UIColor {
        if #available(iOS 13.0, *) {
            return UIColor.systemGray
        } else {
            return .lightGray
        }
    }
    
    static var CTAColor: UIColor {
        if #available(iOS 13.0, *) {
            return UIColor.systemRed
        } else {
            return .red
        }
    }
}
