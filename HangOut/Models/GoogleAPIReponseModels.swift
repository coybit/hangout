//
//  NearByPlacesResponse.swift
//  HangOut
//
//  Created by Mohsen Alijanpour on 15/03/2020.
//  Copyright © 2020 mohsen.dev. All rights reserved.
//

import Foundation

// MARK: - Welcome
struct NearByPlacesResponse: Codable {
    let results: [Place]
    let status: String
    
    enum CodingKeys: String, CodingKey {
        case results, status
    }
}

// MARK: - Result
struct Place: Codable {
    let geometry: Geometry
    let icon: String
    let id, name: String
    let openingHours: OpeningHours?
    let photos: [Photo]
    let placeID, reference: String
    let rating: Double?
    let types: [String]
    let vicinity: String
    
    enum CodingKeys: String, CodingKey {
        case geometry, icon, id, name
        case openingHours = "opening_hours"
        case photos
        case placeID = "place_id"
        case rating
        case reference, types, vicinity
    }
}

// MARK: - Geometry
struct Geometry: Codable {
    let location: Location
}

// MARK: - Location
struct Location: Codable {
    let lat, lng: Double
}

// MARK: - OpeningHours
struct OpeningHours: Codable {
    let openNow: Bool
    
    enum CodingKeys: String, CodingKey {
        case openNow = "open_now"
    }
}

// MARK: - Photo
struct Photo: Codable {
    let height: Int
    let photoReference: String
    let width: Int
    
    enum CodingKeys: String, CodingKey {
        case height
        case photoReference = "photo_reference"
        case width
    }
}

// MARK: - PlaceDetailsResponse
struct PlaceDetailsResponse: Codable {
    let result: PlaceDetails
    let status: String

    enum CodingKeys: String, CodingKey {
        case result, status
    }
}

// MARK: - PlaceDetails
struct PlaceDetails: Codable {
    let reviews: [Review]

    enum CodingKeys: String, CodingKey {
        case reviews
    }
}

// MARK: - Review
struct Review: Codable {
    let authorName: String
    let authorURL: String
    let language: String
    let profilePhotoURL: String
    let rating: Double
    let relativeTimeDescription, text: String
    let time: Int

    enum CodingKeys: String, CodingKey {
        case authorName = "author_name"
        case authorURL = "author_url"
        case language
        case profilePhotoURL = "profile_photo_url"
        case rating
        case relativeTimeDescription = "relative_time_description"
        case text, time
    }
}
