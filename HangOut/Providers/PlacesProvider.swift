//
//  PlacesProvider.swift
//  HangOut
//
//  Created by Mohsen Alijanpour on 18/03/2020.
//  Copyright © 2020 mohsen.dev. All rights reserved.
//


import Foundation
import CoreLocation

protocol PlacesProvider {
    func fetchPlaces(near location: CLLocationCoordinate2D, completion: @escaping (Result<[Place], PlacesProviderError>) -> Void)
    func fetchReviews(of place: Place, completion: @escaping (Result<PlaceDetailsResponse, PlacesProviderError>) -> Void)
}

class JSONPlacesProvider: PlacesProvider {
    
    let repository: PlacesRepository
    
    init(repository: PlacesRepository) {
        self.repository = repository
    }
    
    func fetchPlaces(near location: CLLocationCoordinate2D, completion: @escaping (Result<[Place], PlacesProviderError>) -> Void) {
        DispatchQueue.global().async {
            self.fetchAllInterestingPlaces(near: location, completion: completion)
        }
    }
    
    func fetchReviews(of place: Place, completion: @escaping (Result<PlaceDetailsResponse, PlacesProviderError>) -> Void) {
        repository.reviews(of: place) { response in
            switch response {
            case .success(let placeDetails):
                completion(self.decodeResponse(PlaceDetailsResponse.self, from: placeDetails))
            case .failure(let error):
                completion(.failure(.repositoryError(error)))
            }
        }
    }
    
    private func fetchAllInterestingPlaces(near location: CLLocationCoordinate2D, completion: @escaping (Result<[Place], PlacesProviderError>) -> Void) {
    
        /* Because of repository limitation, we can ask for one type of place each time.
            To solve this problem, we ask the repository 3 times for 2 different types.
            To avoid race-condition, we utilize DispatchSemaphore and because we have to
            wait till all responses are received, we use DispatchGroup.
            By using Future pattern instead of callback we can reduce complexity of this
            piece of code.
         */
        
        let lock = DispatchSemaphore(value: 1)
        let group = DispatchGroup()
        let types: [PlaceType] = [.bar, .restaurant, .cafe]
        var results = [Result<NearByPlacesResponse, PlacesProviderError>]()
        
        for type in types {
            group.enter()
            repository.places(ofType: type, near: location) { response in
                defer { group.leave() }

                let result: Result<NearByPlacesResponse, PlacesProviderError>

                switch response {
                case .success(let data):
                    result = self.decodeResponse(NearByPlacesResponse.self, from: data)
                case .failure(let error):
                    result = .failure(.repositoryError(error))
                }
                
                lock.wait()
                results.append(result)
                lock.signal()
            }
        }
        
        group.notify(queue: .main) {
            guard results.count == types.count else {
                return
            }
            
            let hasAnySuccess = results.contains { if case .success = $0 { return true } else { return false } }
            guard hasAnySuccess else {
                completion(.failure(.unableToGetPlaces))
                return
            }
            
            var allPlaces = [Place]()
            for case .success(let places) in results {
                allPlaces.append(contentsOf: places.results)
            }
            
            completion(.success(allPlaces))
        }
    }
    
    private func decodeResponse<T>(_ type: T.Type, from data: Data) -> Result<T, PlacesProviderError> where T : Decodable {
        do {
            let decoder = JSONDecoder()
            let response = try decoder.decode(type, from: data)
            return .success(response)
        } catch {
            return .failure(.decoderError(error))
        }
    }
}

enum PlacesProviderError: Error {
    case repositoryError(Error)
    case decoderError(Error)
    case unableToGetPlaces
}


