//
//  UserLocationProvider.swift
//  HangOut
//
//  Created by Mohsen Alijanpour on 18/03/2020.
//  Copyright © 2020 mohsen.dev. All rights reserved.
//

import Foundation
import CoreLocation

protocol UserLocationProvider {
    var permissionStatus: PermissionStatus { get }
    func askPermission(completion: @escaping () -> Void)
    func currentUserLocation(completion: @escaping (CLLocationCoordinate2D?) -> Void)
}

enum PermissionStatus {
     case notAsked
     case declined
     case granted
 }

class CLUserLocationProvider: NSObject, UserLocationProvider {
    private let locationManager = CLLocationManager()
    private var authorizationStatusListeners = [() -> Void]()
    private var locationRequest = [(CLLocationCoordinate2D?) -> Void]()
    private var userLocation: CLLocationCoordinate2D? {
        didSet {
            locationRequest.forEach { $0(userLocation) }
            locationRequest.removeAll()
        }
    }
    
    var permissionStatus: PermissionStatus {
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined:
            return .notAsked
        case .restricted, .denied:
            return .declined
        case .authorizedAlways, .authorizedWhenInUse:
            return .granted
        }
    }
    
    override init() {
        super.init()
        
        locationManager.delegate = self
    }
    
    func askPermission(completion: @escaping () -> Void) {
        authorizationStatusListeners.append(completion)
        locationManager.requestWhenInUseAuthorization()
    }
    
    func currentUserLocation(completion: @escaping (CLLocationCoordinate2D?) -> Void) {
        locationManager.requestLocation()
        locationRequest.append(completion)
    }
}

extension CLUserLocationProvider: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        authorizationStatusListeners.forEach { $0() }
        authorizationStatusListeners.removeAll()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        userLocation = locations.first?.coordinate
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        userLocation = nil
    }
}
