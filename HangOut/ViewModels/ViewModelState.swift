//
//  ViewModelState.swift
//  HangOut
//
//  Created by Mohsen Alijanpour on 18/03/2020.
//  Copyright © 2020 mohsen.dev. All rights reserved.
//

import Foundation

enum ViewModelState<T, E> where E: Error {
    case initialized
    case loading
    case loaded(T)
    case error(E)
}
