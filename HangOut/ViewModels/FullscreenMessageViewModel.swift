//
//  FullScreenMessageViewModel.swift
//  HangOut
//
//  Created by Mohsen Alijanpour on 15/03/2020.
//  Copyright © 2020 mohsen.dev. All rights reserved.
//

import UIKit

class FullScreenMessageViewModel {
    let message: String
    let image: UIImage
    let CTAHandler: (() -> Void)?
    let CTATitle: String?
    
    var isCTAVisible: Bool {
        return CTAHandler != nil
    }
    
    init(message: String, image: UIImage, CTATitle: String? = nil, CTAHandler: (() -> Void)? = nil) {
        self.message = message
        self.image = image
        self.CTATitle = CTATitle
        self.CTAHandler = CTAHandler
    }
    
    func CTAButtonDidTap() {
        CTAHandler?()
    }
}
