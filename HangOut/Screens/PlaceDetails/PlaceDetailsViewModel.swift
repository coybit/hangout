//
//  PlaceDetailsViewModel.swift
//  HangOut
//
//  Created by Mohsen Alijanpour on 19/03/2020.
//  Copyright © 2020 mohsen.dev. All rights reserved.
//

import UIKit

enum PlaceDetailsContentViewModel {
    case error(FullScreenMessageViewModel)
    case loading(FullScreenMessageViewModel)
    case loaded(PlaceDetailsLoadedStateViewModel)
    case empty(FullScreenMessageViewModel)
}

protocol PlacesDetailsViewModelDelegate: class {
    func viewModel(_ viewModel: PlaceDetailsViewModel, stateDidChangeTo state: PlaceDetailsViewModel.State, contentViewModel: PlaceDetailsContentViewModel)
}

class PlaceDetailsViewModel {
    typealias State = ViewModelState<PlaceDetails, PlaceDetailsViewModelError>
    
    weak var delegate: PlacesDetailsViewModelDelegate?
    private let provider: PlacesProvider
    private let place: Place
    
    private(set) var state: State {
        didSet {
            notifyDelegateStateDidChange()
        }
    }
    
    var title: String {
        return place.name
    }
    
    var networkIndicator: Bool {
        switch state {
        case .loading:
            return true
        default:
            return false
        }
    }
    

    init(place: Place, provider: PlacesProvider) {
        self.provider = provider
        self.state = .initialized
        self.place = place
    }
    
    func viewDidLoad() {
        loadPlaceDetails()
    }
    
    private func loadPlaceDetails() {
        state = .loading
        
        self.provider.fetchReviews(of: place) { result in
            switch result {
            case .success(let response):
                self.state = .loaded(response.result)
            case .failure(let error):
                self.state = .error(.providerError(error))
            }
        }
    }
    
    private func notifyDelegateStateDidChange() {
        DispatchQueue.main.async { [state] in
            switch state {
            case .error(let error):
                let vm = FullScreenMessageViewModel(message: error.localizedDescription, image: UIImage(named: "city")!, CTATitle: "Retry", CTAHandler: { self.loadPlaceDetails() })
                self.delegate?.viewModel(self, stateDidChangeTo: state, contentViewModel: .error(vm))
            case .loaded(let details):
                let vm = PlaceDetailsLoadedStateViewModel(details: details, provider: self.provider)
                 self.delegate?.viewModel(self, stateDidChangeTo: state, contentViewModel: .loaded(vm))
            case .loading, .initialized:
                let vm = FullScreenMessageViewModel(message: "Loading Place Details ...", image: UIImage(named: "city")!)
                self.delegate?.viewModel(self, stateDidChangeTo: state, contentViewModel: .loading(vm))
            }
        }
    }
    
    enum PlaceDetailsViewModelError: Error {
        case providerError(Error)
        case locationPermissionDeclined
    }
}
