//
//  PlaceDetailsLoadedStateViewModel.swift
//  HangOut
//
//  Created by Mohsen Alijanpour on 19/03/2020.
//  Copyright © 2020 mohsen.dev. All rights reserved.
//

import Foundation

class PlaceDetailsLoadedStateViewModel {
    private let details: PlaceDetails
    private let provider: PlacesProvider
 
    var count: Int {
        return details.reviews.count
    }
    
    init(details: PlaceDetails, provider: PlacesProvider) {
        self.details = details
        self.provider = provider
    }
    
    func review(atRow row: Int) -> ReviewViewModel? {
        guard row < details.reviews.count else { return nil }
        return ReviewViewModel(review: details.reviews[row])
    }
}
