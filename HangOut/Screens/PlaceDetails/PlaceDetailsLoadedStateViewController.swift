//
//  PlaceDetailsLoadedStateViewController.swift
//  HangOut
//
//  Created by Mohsen Alijanpour on 19/03/2020.
//  Copyright © 2020 mohsen.dev. All rights reserved.
//

import UIKit

class PlaceDetailsLoadedStateViewController: ViewModelBasedViewController<PlaceDetailsLoadedStateViewModel> {
    private let ReviewCellIdentifier = "ReviewCellIdentifier"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let tableView = UITableView()
        tableView.dataSource = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: ReviewCellIdentifier)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundColor = Appearance.backgroundColor
        tableView.rowHeight = UITableView.automaticDimension
        tableView.allowsSelection = false
        
        view.addSubview(tableView)
        
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
        ])
    }
}

extension PlaceDetailsLoadedStateViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let place = viewModel.review(atRow: indexPath.row) else {
            preconditionFailure("Invalid Index")
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: ReviewCellIdentifier)!
        cell.textLabel?.text = place.text
        cell.textLabel?.numberOfLines = 0
        cell.backgroundColor = Appearance.backgroundColor
        return cell
    }
}
