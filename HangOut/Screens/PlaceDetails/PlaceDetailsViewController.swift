//
//  PlaceDetailsViewController.swift
//  HangOut
//
//  Created by Mohsen Alijanpour on 18/03/2020.
//  Copyright © 2020 mohsen.dev. All rights reserved.
//

import UIKit

class PlaceDetailsViewController: ViewModelBasedViewController<PlaceDetailsViewModel> {
    private static let photoCellIdentifier = "photoCellIdentifier"
    private var childViewController: UIViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = Appearance.backgroundColor
        
        title = viewModel.title
        
        viewModel.delegate = self
        viewModel.viewDidLoad()
    }
}

extension PlaceDetailsViewController: PlacesDetailsViewModelDelegate {
    func viewModel(_ viewModel: PlaceDetailsViewModel, stateDidChangeTo state: PlaceDetailsViewModel.State, contentViewModel: PlaceDetailsContentViewModel) {

        UIApplication.shared.isNetworkActivityIndicatorVisible = viewModel.networkIndicator
        
        let vc: UIViewController
        
        switch  contentViewModel {
        case .error(let vm):
            vc = FullscreenErrorStateViewController(viewModel: vm)
        case .loaded(let vm):
            vc = PlaceDetailsLoadedStateViewController(viewModel: vm)
        case .loading(let vm):
            vc = FullscreenLoadingStateViewController(viewModel: vm)
        case .empty(let vm):
            vc = FullscreenEmptyStateViewController(viewModel: vm)
        }
        
        childViewController?.didMove(toParent: nil)
        childViewController?.view.removeFromSuperview()
        
        vc.didMove(toParent: self)
        view.addSubview(vc.view)
        
        vc.view.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            vc.view.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            vc.view.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            vc.view.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            vc.view.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
        ])
        
        childViewController = vc
    }
}
