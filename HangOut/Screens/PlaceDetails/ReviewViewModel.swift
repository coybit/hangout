//
//  ReviewViewModel.swift
//  HangOut
//
//  Created by Mohsen Alijanpour on 19/03/2020.
//  Copyright © 2020 mohsen.dev. All rights reserved.
//

import Foundation

class ReviewViewModel {
    private let review: Review
    
    var text: String {
        let rate = String(format: "%.1f/5", review.rating)
        let content = [review.authorName, rate, review.relativeTimeDescription, review.text].joined(separator: "\n")
        
        return content
    }
    
    init(review: Review) {
        self.review = review
    }
}
