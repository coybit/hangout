//
//  PlacesListLoadedStateViewModel.swift
//  HangOut
//
//  Created by Mohsen Alijanpour on 15/03/2020.
//  Copyright © 2020 mohsen.dev. All rights reserved.
//
import Foundation

class PlacesListLoadedStateViewModel {
    private let places: [Place]
    private let navigator: Navigator
    private let provider: PlacesProvider
 
    var count: Int {
        return places.count
    }
    
    init(places: [Place], provider: PlacesProvider, navigator: Navigator) {
        self.places = places
        self.navigator = navigator
        self.provider = provider
    }
    
    func place(atRow row: Int) -> PlaceViewModel? {
        guard row < places.count else { return nil }
        return PlaceViewModel(place: places[row], provider: provider)
    }
    
    func viewDidSelect(itemAtRow row: Int) {
        guard row < places.count else { return }
        
        let vm = PlaceDetailsViewModel(place: places[row], provider: provider)
        navigator.pushViewController(.placeDetails(vm))
    }
}
