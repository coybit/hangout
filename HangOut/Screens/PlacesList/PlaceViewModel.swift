//
//  PlaceViewModel.swift
//  HangOut
//
//  Created by Mohsen Alijanpour on 18/03/2020.
//  Copyright © 2020 mohsen.dev. All rights reserved.
//

import Foundation

class PlaceViewModel {
    private let place: Place
    private let provider: PlacesProvider
    
    var text: String {
        let isOpenNowInfo: String
        if let isOpenNow = place.openingHours?.openNow {
            isOpenNowInfo = isOpenNow ? "Open" : "Close"
        } else {
            isOpenNowInfo = "Contact them to check if they're open"
        }
        
        let rating: String
        if let placeRating = place.rating {
            rating = String(format: "Rate %.1f / 5", placeRating)
        } else {
            rating = "No Rate"
        }
        
        let interestingTypes = ["bar", "restaurant", "cafe"]
        let type = place.types.filter(interestingTypes.contains).joined(separator: ",")
        
        return [place.name,
                type,
                rating,
                place.vicinity,
                isOpenNowInfo
            ].joined(separator: "\n")
    }
    
    init(place: Place, provider: PlacesProvider) {
        self.place = place
        self.provider = provider
    }
}
