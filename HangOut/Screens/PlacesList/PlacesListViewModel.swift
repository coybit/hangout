//
//  PlacesListViewModel.swift
//  HangOut
//
//  Created by Mohsen Alijanpour on 18/03/2020.
//  Copyright © 2020 mohsen.dev. All rights reserved.
//

import UIKit
import CoreLocation

protocol NearbyPlacesViewModelDelegate: class {
    func viewModel(_ viewModel: PlacesListViewModel, stateDidChangeTo state: PlacesListViewModel.State, contentViewModel: PlacesListContentViewModel)
    func viewModel(_ viewModel: PlacesListViewModel, sortDidChangeTo sort: PlacesListViewModel.PlaceSort)
}

enum PlacesListContentViewModel {
    case error(FullScreenMessageViewModel)
    case loading(FullScreenMessageViewModel)
    case loaded(PlacesListLoadedStateViewModel)
    case empty(FullScreenMessageViewModel)
}

class PlacesListViewModel {
    typealias State = ViewModelState<[Place], NearbyPlacesViewModelError>
    
    weak var delegate: NearbyPlacesViewModelDelegate?
    private let provider: PlacesProvider
    private let navigator: Navigator
    private let locationProvider: UserLocationProvider
    private var sort: PlaceSort = .rating {
        didSet {
            delegate?.viewModel(self, sortDidChangeTo: sort)
        }
    }
    
    private(set) var state: State {
        didSet {
            notifyDelegateStateDidChange()
        }
    }
    
    var title: String {
        return "Hang Out!"
    }
    
    var sortTitle: String {
        switch sort {
        case .rating: return "Sort (Rate)"
        case .distance: return "Sort (Closer first)"
        case .isOpenNow: return "Sort (Opens first)"
        }
    }
    
    var networkIndicator: Bool {
        switch state {
        case .loading:
            return true
        default:
            return false
        }
    }
    
    private(set) var userLocation: CLLocationCoordinate2D?
    
    init(provider: PlacesProvider, navigator: Navigator, locationProvider: UserLocationProvider) {
        self.provider = provider
        self.navigator = navigator
        self.state = .initialized
        self.locationProvider = locationProvider
    }
    
    func viewDidLoad() {
        sort = .rating

        loadNearbyPlaces()
    }
    
    func sortDidChange(to newSort: PlaceSort) {
        guard case .loaded(let places) = state else {
            assertionFailure("Can not change sort method until data is loaded")
            return
        }
        
        guard let userLocation = userLocation else {
            assertionFailure("To sort places user location is needed.")
            return
        }
        
        sort = newSort
        state = .loading
        
        state = .loaded(places.sorted(by: { sort.compare(userLocation: userLocation, place1: $0, place2: $1) }))
    }
    
    private func loadNearbyPlaces() {
        switch locationProvider.permissionStatus {
        case .notAsked:
            locationProvider.askPermission { self.loadNearbyPlaces() }
        case .declined:
            state = .error(.locationPermissionDeclined)
            let vm = FullScreenMessageViewModel(message: "Need Location Access! Go to Settings.", image: UIImage(named: "city")!, CTATitle: "Retry", CTAHandler: { self.loadNearbyPlaces() })
            self.delegate?.viewModel(self, stateDidChangeTo: state, contentViewModel: .error(vm))
        case .granted:
            loadPlaceNearUser()
        }
    }
    
    private func loadPlaceNearUser() {
        state = .loading
        
        // ToDo: To avoid call-back hell we can use Future here
        locationProvider.currentUserLocation { userLocation in
            guard let userLocation = userLocation else {
                self.state = .error(.locationPermissionDeclined)
                let vm = FullScreenMessageViewModel(message: "Not able to get user location.", image: UIImage(named: "city")!, CTATitle: "Retry", CTAHandler: { self.loadNearbyPlaces() })
                self.delegate?.viewModel(self, stateDidChangeTo: self.state, contentViewModel: .error(vm))
                return
            }
            
            self.userLocation = userLocation
            
            self.provider.fetchPlaces(near: userLocation) { result in
                switch result {
                case .success(let places):
                    self.state = .loaded(places.sorted(by: { self.sort.compare(userLocation: userLocation, place1: $0, place2: $1) }))
                case .failure(let error):
                    self.state = .error(.providerError(error))
                }
            }
        }
    }
    
    private func notifyDelegateStateDidChange() {
        DispatchQueue.main.async { [state] in
            switch state {
            case .error(let error):
                let vm = FullScreenMessageViewModel(message: error.localizedDescription, image: UIImage(named: "city")!, CTATitle: "Retry", CTAHandler: { self.loadNearbyPlaces() })
                self.delegate?.viewModel(self, stateDidChangeTo: state, contentViewModel: .error(vm))
            case .loaded(let data):
                if data.count > 0 {
                    let vm = PlacesListLoadedStateViewModel(places: data, provider: self.provider, navigator: self.navigator)
                    self.delegate?.viewModel(self, stateDidChangeTo: state, contentViewModel: .loaded(vm))
                } else {
                    let vm = FullScreenMessageViewModel(message: "No Places.", image: UIImage(named: "city")!, CTATitle: "Refresh", CTAHandler: { self.loadNearbyPlaces() })
                    self.delegate?.viewModel(self, stateDidChangeTo: state, contentViewModel: .empty(vm))
                }
            case .loading, .initialized:
                let vm = FullScreenMessageViewModel(message: "Loading Nearby Places ...", image: UIImage(named: "city")!)
                self.delegate?.viewModel(self, stateDidChangeTo: state, contentViewModel: .loading(vm))
            }
        }
    }
    
    enum NearbyPlacesViewModelError: Error {
        case providerError(Error)
        case locationPermissionDeclined
    }
    
    enum PlaceSort {
        case rating
        case distance
        case isOpenNow
        
        func compare(userLocation: CLLocationCoordinate2D, place1: Place, place2: Place) -> Bool {
            switch self {
            case .rating:
                if let rate1 = place1.rating, let rate2 = place2.rating {
                    return rate1 > rate2
                } else if place1.rating == nil, place2.rating != nil {
                    return false
                } else {
                    return true
                }
            case .distance:
                return userLocation.distance(from: place1.geometry.location) < userLocation.distance(from: place2.geometry.location)
            case .isOpenNow:
                let place1IsOpen = place1.openingHours?.openNow ?? false
                let place2IsOpen = place2.openingHours?.openNow ?? false
                return place1IsOpen && !place2IsOpen ? true : false
            }
        }
    }
}

extension CLLocationCoordinate2D {
    func distance(from otherLocation: Location) -> Double {
        let location1 = CLLocation(latitude: latitude, longitude: longitude)
        let location2 = CLLocation(latitude: otherLocation.lat, longitude: otherLocation.lng)
        return location1.distance(from: location2)
    }
}
