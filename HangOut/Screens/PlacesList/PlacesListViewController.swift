//
//  PlacesListViewController.swift
//  HangOut
//
//  Created by Mohsen Alijanpour on 15/03/2020.
//  Copyright © 2020 mohsen.dev. All rights reserved.
//

import UIKit
import CoreLocation

class PlacesListViewController: ViewModelBasedViewController<PlacesListViewModel> {
    var childViewController: UIViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = viewModel.title
        view.backgroundColor = Appearance.backgroundColor
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Sort", style: .plain, target: self, action: #selector(sortButtonDidTap))
        
        viewModel.delegate = self
        viewModel.viewDidLoad()
    }
    
    @objc
    private func sortButtonDidTap() {
        let actionsViewController = UIAlertController(title: "",
                                                      message: "Sort the places by",
                                                      preferredStyle: .actionSheet)
        
        let sortByRateAction = UIAlertAction(title: "Rate",
                                             style: .default,
                                             handler: { _ in self.viewModel.sortDidChange(to: .rating)})
        let sortByDistanceAction = UIAlertAction(title: "Distance",
                                                 style: .default,
                                                 handler: { _ in self.viewModel.sortDidChange(to: .distance)})
        let sortByIsOpenNowAction = UIAlertAction(title: "Open Now",
                                                  style: .default,
                                                  handler: { _ in self.viewModel.sortDidChange(to: .isOpenNow)})
        
        actionsViewController.addAction(sortByRateAction)
        actionsViewController.addAction(sortByDistanceAction)
        actionsViewController.addAction(sortByIsOpenNowAction)

        present(actionsViewController, animated: true, completion: nil)
    }
}

extension PlacesListViewController: NearbyPlacesViewModelDelegate {
    
    func viewModel(_ viewModel: PlacesListViewModel, sortDidChangeTo sort: PlacesListViewModel.PlaceSort) {
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: viewModel.sortTitle, style: .plain, target: self, action: #selector(sortButtonDidTap))
    }
    
    func viewModel(_ viewModel: PlacesListViewModel, stateDidChangeTo state: PlacesListViewModel.State, contentViewModel: PlacesListContentViewModel) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = viewModel.networkIndicator
        
        let vc: UIViewController
        
        switch  contentViewModel {
        case .error(let vm):
            vc = FullscreenErrorStateViewController(viewModel: vm)
        case .loaded(let vm):
            vc = PlacesListLoadedStateViewController(viewModel: vm)
        case .loading(let vm):
            vc = FullscreenLoadingStateViewController(viewModel: vm)
        case .empty(let vm):
            vc = FullscreenEmptyStateViewController(viewModel: vm)
        }
        
        childViewController?.didMove(toParent: nil)
        childViewController?.view.removeFromSuperview()
        
        vc.didMove(toParent: self)
        view.addSubview(vc.view)
        
        vc.view.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            vc.view.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            vc.view.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            vc.view.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            vc.view.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
        ])
        
        childViewController = vc
    }
}
