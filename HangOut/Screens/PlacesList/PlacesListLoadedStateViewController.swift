//
//  PlacesListLoadedStateViewController.swift
//  HangOut
//
//  Created by Mohsen Alijanpour on 15/03/2020.
//  Copyright © 2020 mohsen.dev. All rights reserved.
//

import UIKit

class PlacesListLoadedStateViewController: ViewModelBasedViewController<PlacesListLoadedStateViewModel> {
    private let PlaceCellIdentifier = "PlaceCellIdentifier"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let tableView = UITableView()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: PlaceCellIdentifier)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundColor = Appearance.backgroundColor
        tableView.rowHeight = UITableView.automaticDimension
        
        view.addSubview(tableView)
        
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
        ])
    }
}

extension PlacesListLoadedStateViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let place = viewModel.place(atRow: indexPath.row) else {
            preconditionFailure("Invalid Index")
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: PlaceCellIdentifier)!
        cell.textLabel?.text = place.text
        cell.textLabel?.numberOfLines = 0
        cell.backgroundColor = Appearance.backgroundColor
        return cell
    }
}

extension PlacesListLoadedStateViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.viewDidSelect(itemAtRow: indexPath.row)
    }
}
