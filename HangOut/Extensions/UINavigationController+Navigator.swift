//
//  UINavigationController+Navigator.swift
//  HangOut
//
//  Created by Mohsen Alijanpour on 18/03/2020.
//  Copyright © 2020 mohsen.dev. All rights reserved.
//

import UIKit

enum NavigationDestination {
    case placeDetails(PlaceDetailsViewModel)
}

protocol Navigator {
    func pushViewController(_ destination: NavigationDestination)
}

extension UINavigationController: Navigator {
    func pushViewController(_ destionation: NavigationDestination) {
        
        let vc: UIViewController
        
        switch destionation {
        case .placeDetails(let vm):
            vc = PlaceDetailsViewController(viewModel: vm)
        }
        
        pushViewController(vc, animated: true)
    }
}
