//
//  PlacesRepository.swift
//  HangOut
//
//  Created by Mohsen Alijanpour on 15/03/2020.
//  Copyright © 2020 mohsen.dev. All rights reserved.
//

import Foundation
import CoreLocation

protocol PlacesURLSession {
    func dataTask(with request: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask
}

protocol PlacesRepository {
    func places(ofType type: PlaceType, near location: CLLocationCoordinate2D, completion: @escaping (Result<Data, PlacesRepositoryError>) -> Void)
    func reviews(of place: Place, completion: @escaping (Result<Data, PlacesRepositoryError>) -> Void)
}

class GooglePlacesRepository: PlacesRepository {
    private let apiUrl = "maps.googleapis.com"
    private let urlSession: PlacesURLSession
    
    init(urlSession: PlacesURLSession) {
        self.urlSession = urlSession
    }
    
    func places(ofType type: PlaceType, near location: CLLocationCoordinate2D, completion: @escaping (Result<Data, PlacesRepositoryError>) -> Void) {
        sendRequest(endpoint: "/maps/api/place/nearbysearch/json",
                    method: "GET",
                    queryParams: [
                        URLQueryItem(name: "location", value: "\(location.latitude),\(location.longitude)"),
                        URLQueryItem(name: "type", value: type.stringRepresentation),
                        URLQueryItem(name: "radius", value: "1500"),
                        URLQueryItem(name: "fields", value: "photos,formatted_address,name,rating,opening_hours,geometry")
        ], completion: completion)
    }
    
    func reviews(of place: Place, completion: @escaping (Result<Data, PlacesRepositoryError>) -> Void) {
        sendRequest(endpoint: "/maps/api/place/details/json",
                    method: "GET",
                    queryParams: [
                        URLQueryItem(name: "place_id", value: place.placeID),
                        URLQueryItem(name: "fields", value: "reviews")
        ], completion: completion)
    }
    
    private func sendRequest(endpoint: String, method: String, queryParams: [URLQueryItem], completion: @escaping (Result<Data, PlacesRepositoryError>) -> Void) {
        
        var urlComponent = URLComponents()
        urlComponent.scheme = "https"
        urlComponent.host = apiUrl
        urlComponent.path = endpoint
        
        var queryItems = [
            URLQueryItem(name: "key", value: "AIzaSyDUkbqEMSIvFTWBvf6JbKGTMNGrH2W91Dk")
        ]
        
        queryItems.append(contentsOf: queryParams)
        urlComponent.queryItems = queryItems
        
        guard let url = urlComponent.url else {
            preconditionFailure("Invalid url")
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = method
        
        let task = urlSession.dataTask(with: request) { data, response, error in
            if let error = error {
                completion(.failure(.URLSessionError(error)))
                return
            }
            
            guard let data = data else {
                completion(.failure(.emptyResponse))
                return
            }
            
            completion(.success(data))
        }
        
        task.resume()
    }
}

enum PlacesRepositoryError: Error {
    case URLSessionError(Error)
    case emptyResponse
    case decoderError(Error)
}

enum PlaceType {
    case restaurant
    case cafe
    case bar
    
    var stringRepresentation: String {
        switch self {
        case .restaurant: return "restaurant"
        case .cafe: return "cafe"
        case .bar: return "bar"
        }
    }
}
