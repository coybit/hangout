//
//  AppDelegate.swift
//  HangOut
//
//  Created by Mohsen Alijanpour on 15/03/2020.
//  Copyright © 2020 mohsen.dev. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let navVC = UINavigationController()
        navVC.navigationBar.prefersLargeTitles = true
        
        let repository = GooglePlacesRepository(urlSession: URLSession.shared)
        let viewModel = PlacesListViewModel(provider: JSONPlacesProvider(repository: repository),
                                              navigator: navVC,
                                              locationProvider: CLUserLocationProvider())
        let vc = PlacesListViewController(viewModel: viewModel)
        navVC.viewControllers = [vc]
        
        window?.rootViewController = navVC
        window?.makeKeyAndVisible()
        
        return true
    }
}

