//
//  FullscreenErrorStateViewController.swift
//  HangOut
//
//  Created by Mohsen Alijanpour on 15/03/2020.
//  Copyright © 2020 mohsen.dev. All rights reserved.
//

import UIKit

class FullscreenErrorStateViewController: FullScreenMessageViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    private func setupView() {
        imageView.transform = .init(rotationAngle: -.pi)
    }
}
