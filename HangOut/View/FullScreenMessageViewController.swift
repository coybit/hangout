//
//  FullScreenMessageViewController.swift
//  HangOut
//
//  Created by Mohsen Alijanpour on 15/03/2020.
//  Copyright © 2020 mohsen.dev. All rights reserved.
//

import UIKit

class FullScreenMessageViewController: ViewModelBasedViewController<FullScreenMessageViewModel> {
    let label = UILabel()
    let button = UIButton()
    let imageView = UIImageView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    private func setupView() {
        let stack = UIStackView(arrangedSubviews: [imageView, label, button])
        
        view.addSubview(stack)
        
        NSLayoutConstraint.activate([
            stack.leadingAnchor.constraint(equalTo: view.layoutMarginsGuide.leadingAnchor),
            stack.trailingAnchor.constraint(equalTo: view.layoutMarginsGuide.trailingAnchor),
            stack.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor),
            imageView.heightAnchor.constraint(equalToConstant: 128)
        ])
        
        additionalSafeAreaInsets = .init(top: 36, left: 24, bottom: 36, right: 24)
        
        label.text = viewModel.message
        label.numberOfLines = 0
        label.textAlignment = .center
        label.font = UIFont.preferredFont(forTextStyle: .title3)
        label.textColor = Appearance.ghostColor
        
        imageView.image = viewModel.image
        imageView.contentMode = .scaleAspectFit
        
        button.setTitle(viewModel.CTATitle, for: .normal)
        button.setTitleColor(Appearance.CTAColor, for: .normal)
        button.addTarget(self, action: #selector(CTAButtonDidTap(_:)), for: .touchUpInside)
        button.isHidden = !viewModel.isCTAVisible
        
        stack.axis = .vertical
        stack.alignment = .center
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.spacing = 16
    }
    
    @objc
    private func CTAButtonDidTap(_ sender: UIResponder) {
        viewModel.CTAButtonDidTap()
    }
}
