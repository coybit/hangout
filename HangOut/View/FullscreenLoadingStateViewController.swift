//
//  FullscreenLoadingStateViewController.swift
//  HangOut
//
//  Created by Mohsen Alijanpour on 15/03/2020.
//  Copyright © 2020 mohsen.dev. All rights reserved.
//

import UIKit

class FullscreenLoadingStateViewController: FullScreenMessageViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Add animation
        UIView.animateKeyframes(withDuration: 0.8, delay: 0, options: [.autoreverse, .repeat], animations: {
            self.imageView.alpha = 0.5
        }, completion: nil)
    }
}
