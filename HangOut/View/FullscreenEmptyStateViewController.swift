//
//  FullscreenEmptyStateViewController.swift
//  HangOut
//
//  Created by Mohsen Alijanpour on 15/03/2020.
//  Copyright © 2020 mohsen.dev. All rights reserved.
//

import UIKit

class FullscreenEmptyStateViewController: FullScreenMessageViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageView.alpha = 0.1
    }
}
