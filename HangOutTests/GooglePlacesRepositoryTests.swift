//
//  HangOutTests.swift
//  HangOutTests
//
//  Created by Mohsen Alijanpour on 15/03/2020.
//  Copyright © 2020 mohsen.dev. All rights reserved.
//

import XCTest
import CoreLocation
@testable import HangOut

class GooglePlacesRepositoryTests: XCTestCase {
    let location = CLLocationCoordinate2D(latitude: 52.370216, longitude: 4.895168)
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func test_getNearbyPlaces_correctEndpointIsCalled() {
        let mockURLSession = MockURLSession()
        mockURLSession.data = Data()
        mockURLSession.error = nil
        mockURLSession.response = URLResponse()
        let repository = GooglePlacesRepository(urlSession: mockURLSession)
        
        let exp = expectation(description: "")
        repository.places(ofType: .bar, near: location) { result in
            exp.fulfill()
        }
        
        wait(for: [exp], timeout: 10)
        
        XCTAssertEqual(mockURLSession.request?.url?.path, "/maps/api/place/nearbysearch/json")
    }
    
    func test_getNearbyPlacesGetsError_repositoryReturnsError() {
        let mockURLSession = MockURLSession()
        mockURLSession.data = nil
        mockURLSession.error = NSError()
        mockURLSession.response = URLResponse()
        let repository = GooglePlacesRepository(urlSession: mockURLSession)
        
        let exp = expectation(description: "")
        repository.places(ofType: .bar, near: location) { result in
            guard case .failure = result else {
                XCTFail("Expected to get an error")
                return
            }
            
            exp.fulfill()
        }
        
        wait(for: [exp], timeout: 10)
    }
    
    func test_getNearbyPlacesGetsData_repositoryReturnsData() {
        let mockURLSession = MockURLSession()
        mockURLSession.data = Data()
        mockURLSession.error = nil
        mockURLSession.response = URLResponse()
        let repository = GooglePlacesRepository(urlSession: mockURLSession)
        
        let exp = expectation(description: "")
        repository.places(ofType: .bar, near: location) { result in
            guard case .success = result else {
                XCTFail("Expected to get a response")
                return
            }
            
            exp.fulfill()
        }
        
        wait(for: [exp], timeout: 10)
    }
}

fileprivate class MockURLSession: PlacesURLSession {
    var request: URLRequest?
    var data: Data?
    var response: URLResponse?
    var error: Error?
    
    func dataTask(with request: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        self.request = request
        completionHandler(self.data, self.response, self.error)
        return URLSessionDataTask()
    }
}
