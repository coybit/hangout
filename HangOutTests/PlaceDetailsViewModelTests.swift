//
//  PlaceDetailsViewModelTests.swift
//  HangOutTests
//
//  Created by Mohsen Alijanpour on 19/03/2020.
//  Copyright © 2020 mohsen.dev. All rights reserved.
//

import XCTest
import CoreLocation
@testable import HangOut

class PlaceDetailsViewModelTests: XCTestCase {
    let fakePlace = Place(geometry: .init(location: .init(lat: 0, lng: 0)), icon: "", id: "", name: "", openingHours: nil, photos: [], placeID: "", reference: "", rating: 0, types: [], vicinity: "")

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func test_createNewViewModel_stateIsInitialized() {
        let provider = StubPlacesProvider()
        let vm = PlaceDetailsViewModel(place: fakePlace, provider: provider)
        
        guard case .initialized = vm.state else {
            XCTFail("Wrong state")
            return
        }
    }
    
    func test_viewDidLoadIsCalled_stateIsLoading() {
        let provider = StubPlacesProvider()
        let delegate = SpyViewModelDelegate()
        let vm = PlaceDetailsViewModel(place: fakePlace, provider: provider)
        vm.delegate = delegate
        
        
        let exp = expectation(description: "")
        delegate.stateDidChangeHandler = { _, _ in
            exp.fulfill()
        }
        
        vm.viewDidLoad()
        
        wait(for: [exp], timeout: 10)
        
        guard case .loading = vm.state else {
            XCTFail("Wrong state")
            return
        }
    }

    func test_providerReturnsData_stateIsLoaded() {
        let provider = StubPlacesProvider()
        let delegate = SpyViewModelDelegate()
        let vm = PlaceDetailsViewModel(place: fakePlace, provider: provider)
        vm.delegate = delegate
        
        let exp = expectation(description: "")
        exp.expectedFulfillmentCount = 2
        delegate.stateDidChangeHandler = { _, _ in
            exp.fulfill()
        }
        
        vm.viewDidLoad()
        
        let review = Review(authorName: "", authorURL: "", language: "", profilePhotoURL: "", rating: 0, relativeTimeDescription: "", text: "", time: 0)
        let placeDetails = PlaceDetails(reviews: [review])
        let response = PlaceDetailsResponse(result: placeDetails, status: "")
        
        provider.fetchReviewsCallback?(.success(response))
        
        wait(for: [exp], timeout: 10)
        
        guard case .loaded = vm.state else {
            XCTFail("Wrong state")
            return
        }
    }
}

fileprivate class StubPlacesProvider: PlacesProvider {
    var fetchReviewsCallback: ((Result<PlaceDetailsResponse, PlacesProviderError>) -> Void)?
    
    func fetchPlaces(near location: CLLocationCoordinate2D, completion: @escaping (Result<[Place], PlacesProviderError>) -> Void) {
    }
    
    func fetchReviews(of place: Place, completion: @escaping (Result<PlaceDetailsResponse, PlacesProviderError>) -> Void) {
        fetchReviewsCallback = completion
    }
}

fileprivate class SpyViewModelDelegate: PlacesDetailsViewModelDelegate {
    var stateDidChangeHandler: ((PlaceDetailsViewModel, PlaceDetailsViewModel.State) -> Void)?
    var contentViewModel: PlaceDetailsContentViewModel?
    
    func viewModel(_ viewModel: PlaceDetailsViewModel, stateDidChangeTo state: PlaceDetailsViewModel.State, contentViewModel: PlaceDetailsContentViewModel) {
        self.contentViewModel = contentViewModel
        stateDidChangeHandler?(viewModel, state)
    }
}
