//
//  NearbyPlacesViewModelTests.swift
//  HangOutTests
//
//  Created by Mohsen Alijanpour on 18/03/2020.
//  Copyright © 2020 mohsen.dev. All rights reserved.
//

import XCTest
import CoreLocation
@testable import HangOut

class NearbyPlacesViewModelTests: XCTestCase {
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func test_providerReturnResponse_viewModelChangeStateToLoaded() {
        let provider = StubPlacesProvider()
        let locationProvider = StubAlwaysAllowLocationPermission()
        let vm = PlacesListViewModel(provider: provider, navigator: UINavigationController(), locationProvider: locationProvider)
        
        guard case ViewModelState.initialized = vm.state else {
            XCTFail("Expected to be in `initialized` state")
            return
        }
        
        vm.viewDidLoad()
        
        guard case ViewModelState.loading = vm.state else {
            XCTFail("Expected to be in `loading` state")
            return
        }
        
        provider.fetchPlacesCallback?(.success([]))
        
        guard case ViewModelState.loaded(let data) = vm.state, data.count == 0 else {
            XCTFail("Expected to be in `loading` state")
            return
        }
    }
    
    func test_providerReturnError_viewModelChangeStateToError() {
        let provider = StubPlacesProvider()
        let locationProvider = StubAlwaysAllowLocationPermission()
        let vm = PlacesListViewModel(provider: provider, navigator: UINavigationController(), locationProvider: locationProvider)
        
        guard case ViewModelState.initialized = vm.state else {
            XCTFail("Expected to be in `initialized` state")
            return
        }
        
        vm.viewDidLoad()
        
        guard case ViewModelState.loading = vm.state else {
            XCTFail("Expected to be in `loading` state")
            return
        }
        
        provider.fetchPlacesCallback?(.failure(.decoderError(NSError())))
        
        guard case ViewModelState.error = vm.state else {
            XCTFail("Expected to be in `loading` state")
            return
        }
    }
    
    func test_viewModelChangesState_delegateReceivesNewState() {
        let provider = StubPlacesProvider()
        let locationProvider = StubAlwaysAllowLocationPermission()
        let delegate = SpyViewModelDelegate()
        var states = [String]()
        let exp = expectation(description: "")
        exp.expectedFulfillmentCount = 2
        
        delegate.stateDidChangeHandler = { vm, state in
            switch state {
            case .error: states.append("error")
            case .initialized: states.append("initialized")
            case .loading: states.append("loading")
            case .loaded(_): states.append("loaded")
            }
            exp.fulfill()
        }
        
        let vm = PlacesListViewModel(provider: provider, navigator: UINavigationController(), locationProvider: locationProvider)
        vm.delegate = delegate
        
        vm.viewDidLoad()
        
        sleep(1)
        
        provider.fetchPlacesCallback?(.failure(.decoderError(NSError())))
        
        wait(for: [exp], timeout: 10)
        XCTAssertEqual(states.count, 2)
        XCTAssertEqual(states[0], "loading")
        XCTAssertEqual(states[1], "error")
    }
    
    func test_viewModelGoesToLoading_LoadingVCIsReturned() {
        let provider = StubPlacesProvider()
        let locationProvider = StubAlwaysAllowLocationPermission()
        let delegate = SpyViewModelDelegate()
        let vm = PlacesListViewModel(provider: provider, navigator: UINavigationController(), locationProvider: locationProvider)
        vm.delegate = delegate
        
        let exp = expectation(description: "")
        delegate.stateDidChangeHandler = { _, _ in
            exp.fulfill()
        }
    
        vm.viewDidLoad()
        
        wait(for: [exp], timeout: 10)

        guard case .loading = delegate.contentViewModel else {
            XCTFail("Expected to get .loading")
            return
        }
    }
    
    func test_viewModelGoesToError_ErrorVCIsReturned() {
        let provider = StubPlacesProvider()
        let locationProvider = StubAlwaysAllowLocationPermission()
        let delegate = SpyViewModelDelegate()
        let vm = PlacesListViewModel(provider: provider, navigator: UINavigationController(), locationProvider: locationProvider)
        vm.delegate = delegate
        
        vm.viewDidLoad()
        
        let exp = expectation(description: "")
        exp.expectedFulfillmentCount = 2
        delegate.stateDidChangeHandler = { _, _ in
            exp.fulfill()
        }
        
        provider.fetchPlacesCallback?(.failure(.decoderError(NSError())))
        
        wait(for: [exp], timeout: 10)
        
        guard case .error = delegate.contentViewModel else {
            XCTFail("Expected to get .error")
            return
        }
    }
    
    func test_viewModelGoesToLoaded_EmptyResponse_EmptyVCIsReturned() {
        let provider = StubPlacesProvider()
        let locationProvider = StubAlwaysAllowLocationPermission()
        let delegate = SpyViewModelDelegate()
        let vm = PlacesListViewModel(provider: provider, navigator: UINavigationController(), locationProvider: locationProvider)
        vm.delegate = delegate
        
        vm.viewDidLoad()
        
        let emptyResponse = NearByPlacesResponse(results: [], status: "")
        
        let exp = expectation(description: "")
        exp.expectedFulfillmentCount = 2
        delegate.stateDidChangeHandler = { _, _ in
            exp.fulfill()
        }
        
        provider.fetchPlacesCallback?(.success([]))
        
        wait(for: [exp], timeout: 10)
        
        guard case .empty = delegate.contentViewModel else {
            XCTFail("Expected to get .empty")
            return
        }
    }
    
    func test_viewModelGoesToLoaded_HasResponse_LoadedVCIsReturned() {
        let provider = StubPlacesProvider()
        let locationProvider = StubAlwaysAllowLocationPermission()
        let delegate = SpyViewModelDelegate()
        let vm = PlacesListViewModel(provider: provider, navigator: UINavigationController(), locationProvider: locationProvider)
        vm.delegate = delegate
        
        vm.viewDidLoad()
        
        let exp = expectation(description: "")
        exp.expectedFulfillmentCount = 2
        delegate.stateDidChangeHandler = { _, _ in
            exp.fulfill()
        }
        
        let place = Place(geometry: .init(location: .init(lat: 0, lng: 0)), icon: "", id: "", name: "", openingHours: nil, photos: [], placeID: "", reference: "", rating: 0.0, types: [], vicinity: "")
        provider.fetchPlacesCallback?(.success([place]))
        
        wait(for: [exp], timeout: 10)
        
        guard case .loaded = delegate.contentViewModel else {
            XCTFail("Expected to get .loaded")
            return
        }
    }
    
    func test_locationDenied_contentViewModelIsError() {
        let provider = StubPlacesProvider()
        let locationProvider = StubAlwaysDeclinedLocationPermission()
        let delegate = SpyViewModelDelegate()
        let vm = PlacesListViewModel(provider: provider, navigator: UINavigationController(), locationProvider: locationProvider)
        vm.delegate = delegate
        
        let exp = expectation(description: "")
        delegate.stateDidChangeHandler = { _, _ in
            exp.fulfill()
        }
        
        vm.viewDidLoad()
        
        wait(for: [exp], timeout: 10)
        
        guard case .error = delegate.contentViewModel else {
            XCTFail("Expected to get .loaded")
            return
        }
    }
    
    func test_locationAskedAndDeclined_contentViewModelIsError() {
        let provider = StubPlacesProvider()
        let locationProvider = StubNotAskedButDeclinedLocationPermission()
        let delegate = SpyViewModelDelegate()
        let vm = PlacesListViewModel(provider: provider, navigator: UINavigationController(), locationProvider: locationProvider)
        vm.delegate = delegate
        
        let exp = expectation(description: "")
        delegate.stateDidChangeHandler = { _, _ in
            exp.fulfill()
        }
        
        vm.viewDidLoad()
        
        wait(for: [exp], timeout: 10)
        
        guard case .error = delegate.contentViewModel else {
            XCTFail("Expected to get .loaded")
            return
        }
    }
    
    func test_locationNotAsked_contentViewModelIsLoaded() {
        let provider = StubPlacesProvider()
        let locationProvider = StubNotAskedLocationPermission()
        let delegate = SpyViewModelDelegate()
        let vm = PlacesListViewModel(provider: provider, navigator: UINavigationController(), locationProvider: locationProvider)
        vm.delegate = delegate
        
        let exp = expectation(description: "")
        exp.expectedFulfillmentCount = 2
        delegate.stateDidChangeHandler = { _, _ in
            exp.fulfill()
        }
        
        vm.viewDidLoad()
        
        let place = Place(geometry: .init(location: .init(lat: 0, lng: 0)), icon: "", id: "", name: "", openingHours: nil, photos: [], placeID: "", reference: "", rating: 0.0, types: [], vicinity: "")
        provider.fetchPlacesCallback?(.success([place]))
        
        wait(for: [exp], timeout: 10)
        
        guard case .loaded = delegate.contentViewModel else {
            XCTFail("Expected to get .loaded")
            return
        }
    }
}

fileprivate class StubPlacesProvider: PlacesProvider {
    var fetchPlacesCallback: ((Result<[Place], PlacesProviderError>) -> Void)?
    
    func fetchPlaces(near location: CLLocationCoordinate2D, completion: @escaping (Result<[Place], PlacesProviderError>) -> Void) {
        fetchPlacesCallback = completion
    }
    
    func fetchReviews(of place: Place, completion: @escaping (Result<PlaceDetailsResponse, PlacesProviderError>) -> Void) {
    }
}

fileprivate class SpyViewModelDelegate: NearbyPlacesViewModelDelegate {
    var stateDidChangeHandler: ((PlacesListViewModel, PlacesListViewModel.State) -> Void)?
    var contentViewModel: PlacesListContentViewModel?
    
    func viewModel(_ viewModel: PlacesListViewModel, stateDidChangeTo state: PlacesListViewModel.State, contentViewModel: PlacesListContentViewModel) {
        self.contentViewModel = contentViewModel
        stateDidChangeHandler?(viewModel, state)
    }
    
    func viewModel(_ viewModel: PlacesListViewModel, sortDidChangeTo sort: PlacesListViewModel.PlaceSort) {
    }
}

fileprivate class StubAlwaysAllowLocationPermission: UserLocationProvider {
    var permissionStatus: PermissionStatus = .granted
    
    func askPermission(completion: @escaping () -> Void) {
        completion()
    }
    
    func currentUserLocation(completion: @escaping (CLLocationCoordinate2D?) -> Void) {
        completion(.init(latitude: 0, longitude: 0))
    }
}

fileprivate class StubAlwaysDeclinedLocationPermission: UserLocationProvider {
    var permissionStatus: PermissionStatus = .declined
    
    func askPermission(completion: @escaping () -> Void) {
        completion()
    }
    
    func currentUserLocation(completion: @escaping (CLLocationCoordinate2D?) -> Void) {
        completion(nil)
    }
}

fileprivate class StubNotAskedLocationPermission: UserLocationProvider {
    var permissionStatus: PermissionStatus = .notAsked
    
    func askPermission(completion: @escaping () -> Void) {
        permissionStatus = .granted
        completion()
    }
    
    func currentUserLocation(completion: @escaping (CLLocationCoordinate2D?) -> Void) {
        completion(.init(latitude: 0, longitude: 0))
    }
}

fileprivate class StubNotAskedButDeclinedLocationPermission: UserLocationProvider {
    var permissionStatus: PermissionStatus = .notAsked
    
    func askPermission(completion: @escaping () -> Void) {
        permissionStatus = .declined
        completion()
    }
    
    func currentUserLocation(completion: @escaping (CLLocationCoordinate2D?) -> Void) {
        completion(.init(latitude: 0, longitude: 0))
    }
}
