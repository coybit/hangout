//
//  JSONPlacesProviderTests.swift
//  HangOutTests
//
//  Created by Mohsen Alijanpour on 19/03/2020.
//  Copyright © 2020 mohsen.dev. All rights reserved.
//

import XCTest
import CoreLocation
@testable import HangOut

class JSONPlacesProviderTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func test_repositoryReturnsError_providerReturnsError() {
        let repository = StubRepositoryAlwaysFail()
        let provider = JSONPlacesProvider(repository: repository)
        let exp = expectation(description: "")
        
        provider.fetchPlaces(near: .init(latitude: 0, longitude: 0)) { result in
            defer { exp.fulfill() }
            
            guard case .failure = result else {
                XCTFail("Expected to get failure")
                return
            }
        }
        
        wait(for: [exp], timeout: 10)
    }
    
    func test_repositoryReturnsPlaces_providerReturnsPlaces() {
        let repository = StubRepositoryAlwaySuccess()
        let provider = JSONPlacesProvider(repository: repository)
        let exp = expectation(description: "")
        
        provider.fetchPlaces(near: .init(latitude: 0, longitude: 0)) { result in
            defer { exp.fulfill() }
            
            guard case .success = result else {
                XCTFail("Expected to get failure")
                return
            }
        }
        
        wait(for: [exp], timeout: 10)
    }
}

fileprivate class StubRepositoryAlwaysFail: PlacesRepository {
    func places(ofType type: PlaceType, near location: CLLocationCoordinate2D, completion: @escaping (Result<Data, PlacesRepositoryError>) -> Void) {
        completion(.failure(.decoderError(NSError())))
    }
    
    func reviews(of place: Place, completion: @escaping (Result<Data, PlacesRepositoryError>) -> Void) {
    }
}

fileprivate class StubRepositoryAlwaySuccess: PlacesRepository {
    func places(ofType type: PlaceType, near location: CLLocationCoordinate2D, completion: @escaping (Result<Data, PlacesRepositoryError>) -> Void) {
        
        let json = """
        {
           "results" : [
              {
                 "geometry" : {
                    "location" : {
                       "lat" : -33.870775,
                       "lng" : 151.199025
                    }
                 },
                 "icon" : "http://maps.gstatic.com/mapfiles/place_api/icons/travel_agent-71.png",
                 "id" : "21a0b251c9b8392186142c798263e289fe45b4aa",
                 "name" : "Rhythmboat Cruises",
                 "opening_hours" : {
                    "open_now" : true
                 },
                 "photos" : [
                    {
                       "height" : 270,
                       "html_attributions" : [],
                       "photo_reference" : "CnRnAAAAF-LjFR1ZV93eawe1cU_3QNMCNmaGkowY7CnOf-kcNmPhNnPEG9W979jOuJJ1sGr75rhD5hqKzjD8vbMbSsRnq_Ni3ZIGfY6hKWmsOf3qHKJInkm4h55lzvLAXJVc-Rr4kI9O1tmIblblUpg2oqoq8RIQRMQJhFsTr5s9haxQ07EQHxoUO0ICubVFGYfJiMUPor1GnIWb5i8",
                       "width" : 519
                    }
                 ],
                 "place_id" : "ChIJyWEHuEmuEmsRm9hTkapTCrk",
                 "reference" : "CoQBdQAAAFSiijw5-cAV68xdf2O18pKIZ0seJh03u9h9wk_lEdG-cP1dWvp_QGS4SNCBMk_fB06YRsfMrNkINtPez22p5lRIlj5ty_HmcNwcl6GZXbD2RdXsVfLYlQwnZQcnu7ihkjZp_2gk1-fWXql3GQ8-1BEGwgCxG-eaSnIJIBPuIpihEhAY1WYdxPvOWsPnb2-nGb6QGhTipN0lgaLpQTnkcMeAIEvCsSa0Ww",
                 "types" : [ "travel_agency", "restaurant", "food", "establishment" ],
                 "vicinity" : "Pyrmont Bay Wharf Darling Dr, Sydney"
              },
           ],
           "status" : "OK"
        }
        """
        
        completion(.success(json.data(using: .utf8)!))
    }
    
    func reviews(of place: Place, completion: @escaping (Result<Data, PlacesRepositoryError>) -> Void) {
    }
}
